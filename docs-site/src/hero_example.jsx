import React from 'react'
import DatePicker from 'react-datepicker'
import moment from 'moment'

export default React.createClass({
	displayName: 'HeroExample',

	getInitialState () {
		return {
			startDate: moment()
		}
	},
	getDefaultProps: function () {
    return {
    	locale:"sv-se",
			dateFormat:"YYDDMM",
			popoverAttachment: "top center",
			popoverTargetAttachment: "bottom center",
			popoverTargetOffset:"0px 0px",
			onChangeCallback:()=>{}, 
		}
  },

	handleChange (date) {
		this.setState({
			startDate: date
		})
	},

	render () {
		return <DatePicker
						locale={this.props.locale}
						todayButton={"Idag"}
						dateFormat={"YYMMDD"}
						selectButton={"Select"}
						cancelButton={"Cancel"}
						selected={this.state.startDate}
						popoverAttachment={this.props.popoverAttachment}
						popoverTargetAttachment={this.props.popoverTargetAttachment}
						popoverTargetOffset={this.props.popoverTargetOffset}
						onChange={this.handleChange}/>
	}



})
